<?php
    namespace Entities;

    require_once "AbstractMenu.php";

    class CourseMeal extends AbstractMenu
    {
        /**
         * @var string Name of the meal
         */
        protected string $name;

        /**
         * @var int Price of the meal
         */
        protected int $price;
    
        /**
         * Composes name and price of the meal into string
         * 
         * @return string Formated string containing name and price of the meal
         */
        public function compose(): string
        {
            return $this -> name . ' ' . $this -> price . ' Kč';
        }
    }