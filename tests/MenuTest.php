<?php
    use PHPunit\Framework\TestCase;

    require_once __DIR__ . "/../src/entities/Menu.php";

    use Entities\Menu;
    use Entities\MenuCourse;
    use Entities\CourseMeal;

    final class MenuTest extends TestCase
    {
        public function testComposeCourseMeal()
        {
            $courseMeal = (object)[
                'name'  => 'Polévka',
                'price' => 26,
            ];
            $expected = 'Polévka 26 Kč';
            $this -> assertSame((new CourseMeal($courseMeal)) -> compose(), $expected);

            return (object) [
                'source' => $courseMeal,
                'expected' => $expected
            ];
        }

        /**
         * @depends testComposeCourseMeal
         */
        public function testComposeMenuCourse($courseMealData)
        {
            $menuCourse = (object) [
                'course' => 'Hlavní jídlo',
                'meals' => [$courseMealData -> source],
                'extended' => 'anything',
            ];
            $expected = 'Hlavní jídlo<br>' . $courseMealData -> expected . '<br><br>';
            $this -> assertSame((new MenuCourse($menuCourse)) -> compose(), $expected);

            return (object) [
                'source' => $menuCourse,
                'expected' => $expected
            ];
        }

        /**
         * @depends testComposeMenuCourse
         */
        public function testComposeMenu($menuCourseData)
        {
            $menu = (object) [
                'date' => '1. 1. 2020',
                'courses' => [$menuCourseData -> source],
                'note' => 'Dnes sleva 10 % na čokoládu.',
            ];
            $expected = '1. 1. 2020<br><br>' . $menuCourseData -> expected . 'Dnes sleva 10 % na čokoládu.<br>';
            $this -> assertSame((new Menu($menu)) -> compose(), $expected);

            return (object) [
                'source' => $menu,
                'expected' => $expected
            ];
        }

        /**
         * @depends testComposeMenu
         */
        public function testPrintMenu($menuData)
        {
            $menu = $menuData -> source;
            $expected = $menuData -> expected;
            $this -> expectOutputString($expected);

            (new Menu($menu)) -> render();
        }
    }