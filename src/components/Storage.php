<?php
class Storage
{
        /**
         * @var \MongoDB\Driver\Manager Holds the database connection
         */
	private \MongoDB\Driver\Manager $client;

        /**
         * Initiates the database connection
         */
        public function __construct()
        {
                $this -> client = new \MongoDB\Driver\Manager('mongodb://localhost:27017');
        }

        /**
         * Stores the email and restaurant selection for sending their menu
         * 
         * @param string $email         Visitor's emai
         * @param int[] $selection      Ids of restaurants which menu is sent to email
         * 
         * @return bool If the data were successfully stored into database
         */
	public function store(string $email, array $selection)
	{
		// Sem by bylo vhodné dat validaci
                // Nyní je spoléháno na to, že validaci emailu provádí browser

		$bulk = new \MongoDB\Driver\BulkWrite;

		$record = new \stdClass;
		$record -> email = $email;
		$record -> selection = $selection;

                $_1 = $bulk -> insert($record);

                try
                {
                        $dbRet = $this -> client -> executeBulkWrite('restaurants.sendMenus', $bulk);
                }
                catch (\MongoDB\Driver\Exception\BulkWriteException $e)
                {
                        return false;
                }

                return true;
	}

        /**
         * Gets iterator of stored email addresses and restaurant selections
         * 
         * @return MongoDB\Driver\Cursor
         */
	public function getEmailDBCursor()
	{
		$filter  = [];
                $query = new \MongoDB\Driver\Query($filter);
                $cursor = $this -> client -> executeQuery('restaurants.sendMenus', $query);
		return $cursor;
	}
}
