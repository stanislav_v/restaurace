<?php
    require_once "src/components/Restaurants.php";
    require_once "src/components/Menus.php";
    require_once "src/components/Form.php";
    require_once "src/components/Storage.php";

    class App
    {
        /**
         * Handles inputs and decides what parts of the page are rendered
         */
        public function run(): void
        {
            $message = '';
            if (isset($_POST['email']) && isset($_POST['click']) && $_POST['email'] && $_POST['click'])
                $message = 'Nevybrali jste žádnou restauraci';
            if (isset($_POST['selectedRestaurants']) && isset($_POST['email']) && isset($_POST['click']) && (is_array($_POST['selectedRestaurants'])) && $_POST['email'] && $_POST['click'])
            {
                if (count($_POST['selectedRestaurants']) > 0)
                {
                    $storage = new Storage;
                    $stored = $storage -> store($_POST['email'], $_POST['selectedRestaurants']);
                    if ($stored)
                        $message = 'Jídelní lístky Vámi vybraných restaurací budou zasílány na váš email';
                    else
                        $message = 'Došlo k chybě';
                }
                else
                {
                    $message = 'Nevybrali jste žádnou restauraci';
                }
            }
            
            echo('<div class="list list_r">');
            $restaurants = new Restaurants;
            renderSelectForm($restaurants, $message);
            echo('</div>');
            
            if (isset($_GET['r']) && (!is_null($_GET['r'])))
            {
                $menus = new Menus((int) $_GET['r']);
                echo('<div class="list list_j">');
                echo('Jídelní lístek');
                $menus -> render();
                echo('</div>');
            }
        }

        /**
         * Invokes running the application
         */
        public function __invoke(): void
        {
            $this -> run();
        }
    }