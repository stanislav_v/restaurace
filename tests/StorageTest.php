<?php
    use PHPunit\Framework\TestCase;

    require_once __DIR__ . "/../src/components/Storage.php";

    class StorageTest extends TestCase
    {
        public function testDBCursorType()
        {
            $this -> assertInstanceOf(\MongoDB\Driver\Cursor::class, (new Storage) -> getEmailDBCursor());
        }
    }