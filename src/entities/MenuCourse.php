<?php
    namespace Entities;

    require_once "AbstractMenu.php";

    class MenuCourse extends AbstractMenu
    {
        /**
         * @var string Name of the menu course
         */
        protected string $course;

        /**
         * @var CourseMeal[] Course meals contained in the course
         */
        protected array $meals;

        /**
         * Composes menu course into HTML string
         * 
         * @return string HTML formatted menu course
         */
        public function compose(): string
        {
            $text = '';
            $text .= $this -> course;
            $text .= '<br>';
            foreach($this -> meals as $meal)
            {
                $text .= $meal -> compose();
                $text .= '<br>';
            }
            $text .= '<br>';

            return $text;
        }
    }