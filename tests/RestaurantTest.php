<?php
        use PHPunit\Framework\TestCase;

        require_once __DIR__ . "/../src/entities/Restaurant.php";

        use Entities\Restaurant;

        final class RestaurantTest extends TestCase
        {
                /**
                 * @dataProvider restaurantDescriptionProvider
                 */
                public function testDescriptionCompose(Restaurant $r, string $s): void
                {

                        $this -> assertSame($r -> compose(), $s);
                }

                public function restaurantDescriptionProvider(): array
                {
                        return [
                                [ new Restaurant((object)[
                                'id' => 5,
                                        'name' => 'Restaurace',
                                        'address' => 'U nováků 5']),
                                'Restaurace (U nováků 5)' ],
                                [ new Restaurant((object)[
                                'id' => 8,
                                        'name' => 'U nás',
                                        'address' => 'Praha 5']),
                                'U nás (Praha 5)' ]
                        ];
                }

                /**
                 * @dataProvider restaurantIdProvider
                 */
                public function testIdSet(Restaurant $r, int $id): void
                {

                        $this -> assertSame($r -> getId(), $id);
                }

                public function restaurantIdProvider(): array
                {
                        return [
                                [ new Restaurant((object)[
                                'id' => 5,
                                        'name' => 'Restaurace',
                                        'address' => 'U nováků 5']),
                                5 ],
                                [ new Restaurant((object)[
                                'id' => 8,
                                        'name' => 'U nás',
                                        'address' => 'Praha 5']),
                                8 ]
                        ];
                }
        }
