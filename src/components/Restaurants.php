<?php
require_once "ApiReader.php";
require_once __DIR__ . "/../entities/Restaurant.php";

use Entities\Restaurant;

class Restaurants
{
	/**
	 * @var Entities\Restaurant[] List of restaurants
	 */
	private array $restaurants = [];

	/**
	 * Constructs the list of restaurants
	 */
	public function __construct()
	{
		$restaurants = ApiReader::getRestaurants();
		foreach ($restaurants as $rest)
		{
			if ($rest instanceof Restaurant)
				$this -> restaurants[] = $rest;
		}

	}

	/**
	 * Renders restaurant information
	 */
	public function render(): void
	{
		foreach($this -> restaurants as $rest)
		{
			echo('<br>'."\n");
			$rest();
		}
	}
}
