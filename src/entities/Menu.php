<?php
	namespace Entities;

	require_once "AbstractMenu.php";

	class Menu extends AbstractMenu
	{
		/**
		 * @var string Date of the menu
		 */
		protected string $date;

		/**
		 * @var MenuCourse[] Meal courses contained in the menu
		 */
		protected array $courses;

		/**
		 * @var string A note to the menu
		 */
		protected string $note;

		/**
		 * Composes Menu into HTML string
		 * 
		 * @return string HTML formatted menu
		 */
		public function compose(): string
		{
			$text = '';
			$text .= $this -> date;
			$text .= '<br><br>';
			foreach($this -> courses as $course)
				$text .= $course -> compose();
			$text .= $this -> note;
			$text .= '<br>';

			return $text;
		}

		/**
		 * Renders HTML formatted menu
		 */
		public function render(): void
		{
			echo($this -> compose());
		}
	}

