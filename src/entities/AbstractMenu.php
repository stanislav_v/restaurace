<?php
    namespace Entities;

    require_once "MenuCourse.php";
    require_once "CourseMeal.php";

    abstract class AbstractMenu
    {
        const _ = ['courses' => 'Entities\MenuCourse', 'meals' => 'Entities\CourseMeal'];
    
        /**
         * Constructs the menu from source data
         * 
         * @param object $obj Source data
         */
        public function __construct(object $obj)
            {
                    foreach($obj as $prop => $value)
                    {
                            if(property_exists($this, $prop))
                {
                    if ((is_array($value)) && (array_key_exists($prop, self::_)))
                    {
                        $newValue = [];
                        foreach($value as $v)
                        {
    
                            $class = self::_[$prop];
                            $newValue[] = new $class($v);
                        }
                        $value = $newValue;
                    }
                                    $this -> {$prop} = $value;
                }
                    }
            }
    
    }