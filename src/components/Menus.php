<?php
require_once "ApiReader.php";
require_once __DIR__ . "/../entities/Menu.php";

use Entities\Menu;

class Menus
{
	/**
	 * @var Entities\Menu[] List of menus
	 */
	private array $menus = [];

	/**
	 * Construct the list of menus
	 * 
	 * @param int $rId Restaurant id
	 */
	public function __construct(int $rId)
	{
		$menus = ApiReader::getMenu($rId);
		foreach ($menus as $menu)
		{
			if ($menu instanceof Menu)
					$this -> menus[] = $menu;
		}
	}

	/**
	 * Renders menus
	 */
	public function render(): void
	{
		foreach($this -> menus as $menu)
		{
			echo('<br>'."\n");
			$menu -> render();
		}
	}
}
