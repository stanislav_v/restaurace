<?php
	namespace Entities;
	
	class Restaurant
	{
		/**
		 * @var int ID of the restaurant
		 */
		private int $id;

		/**
		 * @var string Name of the restaurant
		 */
		private string $name;

		/**
		 * @var string  Address of the restaurant
		 */
		private string $address;

		/**
		 * Construct the restaurant from source data
		 * 
		 * @param object $obj Source data
		 */
		public function __construct(object $obj)
		{
			foreach($obj as $prop => $value)
			{
				if(property_exists($this, $prop))
					$this -> {$prop} = $value;
			}
		}

		/**
		 * Returns id of the restaurant
		 * 
		 * @returns string
		 */
		public function getId(): int
		{
			return $this -> id;
		}

		/**
		 * Composes restaurant data into HTML string
		 * 
		 * @param bool $withLink	If link to the restaurant menu is added
		 * @param bool $asForm		If the checkbox is added in the front of the name
		 * 
		 * @return string HTML formatted name and address of the restaurant
		 */
		public function compose(bool $withLink = false, bool $asForm = false): string
		{
			$text = '';
			if ($asForm)
				$text .= '<input type="checkbox" name="selectedRestaurants[' . $this -> id . ']" value="' . $this -> id . '" />';
			if ($withLink)
				$text .= '<a href="?r=' . $this -> id .  '">';
			$text .= $this -> name;
			if ($withLink)
				$text .= '</a>';
			$text .= ' (';
				$text .= $this -> address;
			$text .= ')';

			return $text;
		}

		/**
		 * Renders HTML formatted name and address of the restaurant
		 * 
		 * @param bool $withLink	If link to the restaurant menu is added
		 * @param bool $asForm		If the checkbox is added in the front of the name
		 */
		public function render(bool $withLink = false, bool $asForm = false): void
		{
			echo($this -> compose($withLink, $asForm));
		}

		/**
		 * Invokes rendering with link to the restaurant menu and checkbox
		 */
		public function __invoke()
		{
			$this -> render(true, true);
		}
	}
