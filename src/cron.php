<?php
require_once "components/ApiReader.php";
require_once "components/Storage.php";

/**
 * Script retrieving emails from the database and sending the menu to those emails
 */

const DONT_RUN_FROM_BROWSER = true;

$print_results = false;
if (array_key_exists(1, $argv))
	if('true' === $argv[1])
		$print_results = true;

if(!isset($argv)) // running from browser
	$print_results = true;

if((DONT_RUN_FROM_BROWSER) && (!isset($argv)))
{
	echo('Cannot run from browser');
	exit(1);
}

$headers = [];
$headers[] = 'MIME-Version: 1.0';
$headers[] = 'Content-type: text/html; charset=iso-8859-1';

if(defined('STDOUT'))
	fwrite(STDOUT, 'Downloading restaurants' . "\n");

$restaurants = ApiReader::getRestaurants();
$menus = new \stdClass;

foreach($restaurants as $restaurant)
{
	$rId = $restaurant -> getId();
	if(defined('STDOUT'))
        	fwrite(STDOUT, 'Downloading menu' . $rId . "\n");

	$menus->{'_' . $rId} = ApiReader::getMenu($rId);
}

$storage = new Storage;
$emails = $storage -> getEmailDBCursor();
foreach ($emails as $email)
{

	if(defined('STDOUT'))
		fwrite(STDOUT, 'Sending email to: ' . $email -> email . "\n");

	$htmlEmail = '';
	foreach($email -> selection as $selection)
	{
		$selection = (int) $selection;
		foreach($restaurants as $restaurant)
		{
			if($restaurant -> getId() === $selection)
			{
				$htmlEmail .= $restaurant -> compose();
				break;
			}
		}

		foreach($menus->{'_' . $selection} as $menu)
		{
			$htmlEmail .= '<br>';
			$htmlEmail .= $menu -> compose();
		}
		$htmlEmail .= '<br><br>';

	}

	$htmlEmailMessage = '<html>'
		. '<head><title>Jídelní lístek</title></head>'
		. '<body>' . $htmlEmail . '</body>'
		. '</html>';

	// Odesílání emailů cíleně zakomentováno
	/*mail(
		$email -> email,
		'Jídelní lístek',
		$htmlEmailMessage,
		implode("\r\n", $headers),
		'-f jidelní-listek@jidelni-listek.cz'
	);*/

	if($print_results)
	{
		echo('===' . $email -> email . '===');
		echo('<br>');
		echo($htmlEmail);
		echo('<br><br>');
	}
}


echo('OK');
