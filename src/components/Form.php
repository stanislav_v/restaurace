<?php
require_once "Restaurants.php";

/**
 * Renders form containing list of restarants
 * 
 * @param Restaurants $restaurants	Restaurant list
 * @param string $message			Message to provide feedback to the page visitor
 */
function renderSelectForm(Restaurants $restaurants, string $message = ''): void
{
	echo('<form method="POST">');
	$restaurants -> render();
	echo('<br><br>');
	// type email sice zajistí nějakou validaci emailu
	// ale to nezabrání podvržení dat, takže by byla potřebná
	// validace emailu před zápisem do databáze
	if ($message)
	{
		echo($message);
		echo('<br>');
	}
	echo('<input type="email" name="email"/>');
	echo('<input type="submit" name="click" value="Přihlásit se k odběru jídelních lístků" />');
	echo('</form>');
}
