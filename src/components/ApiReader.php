<?php
require_once __DIR__ . "/../entities/Restaurant.php";
require_once __DIR__ . "/../entities/Menu.php";

// https://idcrestaurant.docs.apiary.io/

class ApiReader
{
	const APIURL = 'https://private-anon-698f635197-idcrestaurant.apiary-mock.com/';

	/**
	 * Gets source data about the restaurants
	 * 
	 * @return object Source restaurant data
	 */
	protected static function getRestaurantData()
	{
		// metoda je get a API nevyzaduje nastavit hlavičky
		// místo culr tak stačí file_get_contents
		$json = file_get_contents(static::APIURL . 'restaurant');
		$restaurants = json_decode($json);

		return $restaurants;
	}

	/**
	 * Returns the Restaurants
	 * 
	 * @return Entities\Restaurant[]|null
	 */
	public static function getRestaurants()
	{
		$restaurantData = self::getRestaurantData();

		if (! $restaurantData)
			return  null;

		$restaurants = [];

		foreach($restaurantData as $rest)
		{
			$restaurants[] = new Entities\Restaurant($rest);
		}

		return $restaurants;
	}

	/**
	 * Gets menu source data
	 * 
	 * @return object  Source menu data
	 */
	protected static function getMenuData(int $rId)
	{
		$json = file_get_contents(static::APIURL . 'daily-menu?restaurant_id=' . $rId);
		$menu = json_decode($json);

		return $menu;

	}

	/**
	 * Returns the Menu
	 * 
	 * @return Entities\Menu[]|null
	 */
	public static function getMenu(int $r)
	{
		$menuData = self::getMenuData($r);
		if (! $menuData)
			return null;

		$menus = [];
		foreach($menuData as $m)
		{
			$menus[] = new Entities\Menu($m);
		}
		return $menus;
	}
}
