<?php
    use PHPunit\Framework\TestCase;

    require_once __DIR__ . "/../src/components/ApiReader.php";
    require_once __DIR__ . "/../src/entities/Menu.php";

    class ApiReaderTest extends TestCase
    {
        public function testGetRestaurants()
        {
            $this -> assertContainsOnlyInstancesOf(Entities\Restaurant::class, $restaurants = ApiReader::getRestaurants() );

            return $restaurants;
        }

        /**
         * @depends testGetRestaurants
         */
        public function testGetMenu(array $restaurants)
        {
            foreach($restaurants as $restaurant)
            {
                $this -> assertContainsOnlyInstancesOf(Entities\Menu::class, ApiReader::getMenu($restaurant -> getId()) );
            }
        }
    }